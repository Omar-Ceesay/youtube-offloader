const express = require('express');
const app = express();
const http = require('http').Server(app);
const port = process.env.PORT || 8080;
const path = require('path');
const mongo = require('mongodb');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const dbUrl = process.env.dbUrl || 'mongodb://127.0.0.1/OffTube';

mongoose.connect(dbUrl, { useFindAndModify: false, useNewUrlParser: true, useUnifiedTopology: true }, function(err, response){
    if(err){
        console.log('failed to connect to ' + dbUrl);
    } else {
        console.log('Connected to ' + dbUrl);
    }
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname + '/public')));
app.use(session({secret: 'anystringoftext',
    saveUninitialized: true,
    resave: true,
    store: new MongoStore({ 
        mongooseConnection: mongoose.connection,
        ttl: 50 * 24 * 60 * 60 // time to live 50 days. (Default is 14 days)
    })
}));
app.use(passport.initialize());
app.use(passport.session());

// Middleware that passes the variable "login" to each page
let auth = require("./middleware/auth");
app.use(auth.isLoggedInVar)
app.use(auth.sendUserInfo)

require('./config/passport')(passport);

app.set('view engine', 'ejs');

// Index route
const index = express.Router();
require("./routes/index.js")(index, passport);
app.use("/", index);

// YouTube route
const youtube = express.Router();
require("./routes/youtube.js")(youtube);
app.use("/youtube", youtube);

// Playlist route
const playlist = express.Router();
require("./routes/playlist.js")(playlist);
app.use("/playlist", playlist);

try {

    http.listen(port);
    console.log('Server running on port: ' + port);
    
} catch (error) {

    console.error("Server failed to start!");
    console.error(error);
    
}