var mongoose = require('mongoose');

var playlistSchema = mongoose.Schema({
    playlistInfo: {
        name: String,
        videoCount: Number
    },
    items: Array,
    owner: {
        email: String,
        username: String,
        id: String
    },
    createdOn: String,
    visibility: String
});

module.exports = mongoose.model('Playlist', playlistSchema);