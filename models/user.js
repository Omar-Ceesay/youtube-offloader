var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
        username: String,
		password: String,
        email: String,
        createdOn: String
});

module.exports = mongoose.model('User', userSchema);