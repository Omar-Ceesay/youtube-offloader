var mongoose = require('mongoose');

var videoSchema = mongoose.Schema({
        count: Number,
		videoId: String,
        title: String
});

module.exports = mongoose.model('Video', videoSchema);