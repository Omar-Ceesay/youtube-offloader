function addPlaylist() {
    
}

$("#create-playlist-form").submit(function(e) {
    e.preventDefault();    
    if($("[name='playlistName']").val().length === 0) {
        M.toast({html: "You must create name for the new playlist"});
    } else {
        $.post("/createplaylist", {playlistName: $("[name='playlistName']").val()}, function(data) {
            M.toast({html: data.message});
            $("[name='playlistName']").val("");
        })
    }
})