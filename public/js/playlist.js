var audio;
var playlist;
var tracks;
var current;
pathArray = window.location.pathname.split('/');
var playlistId = pathArray[pathArray.length - 1];

$(document).ready(function () {
    if (typeof $('#player')[0] !== "undefined") {
        init();
    }
    function init() {
        var current = 0;
        var audio = $('#player');
        var playlist = $('#playlist');
        tracks = playlist.find('li span');
        len = tracks.length - 1;
    
        attachRemoveEventListeners(playlist);

        playlist.find('.video-title').click(function(e){
            e.preventDefault();
            
            link = $(this);
            current = link.parent().parent().index();
            run($(link), audio[0]);
        });

        audio[0].addEventListener('ended',function(e){
            current++;
            if(current > len){
                current = 0;
                link = playlist.find('span')[0];
            }else{
                link = playlist.find('span')[current];    
            }
            run($(link),audio[0]);
        });

        var el = document.getElementById('playlist');
        var dragGhost = {};
        var sortable = Sortable.create(el, {
            animation: 150,
            chosenClass: "highlight-chosen",
            filter: ".ignore-drag",
            dragClass: "sortable-drag",
            handle: ".handler",
            onSort: function (/**Event*/evt) {
                sort();
            }
        });
        
        function sort() {
            list = playlist.find('li');
            items = [];
            var count = 0;
            for (let i = 0; i < list.length; i++) {
                items.push($(list[i]).attr("title"));
                // DON'T MAKE THIS IF/ELSE
                if (i === list.length-1) {
                    dataToSend = {playlistId: playlistId, items: items};
                    $.post("/"+pathArray[1]+"/"+playlistId+"/sort", dataToSend, function(data) {
                        if (!data.success) {
                            M.toast({html: data.message})
                        }
                    })
                }

                if ($(list[i]).attr("class").includes("active")) {
                    current = $(list[i]).index();
                }
            }

        }

    }

    
    function run(link, player){    
        player.src = link.attr('href');
        par = link.parent().parent();
        par.addClass('active').siblings().removeClass('active');
        $(".current-title").text(link.text()); // Change title to current playlist-item
        player.load();
        player.play();
    }

    // Edit playlist name or visibility
    $("#edit-submit").click(function(e) {
        e.preventDefault();
        M.Toast.dismissAll();
        var undoFlag = false;

        var oldName = $("#playlistName").text();
        $("#playlistName").text($("#playlist-name-input").val());

        toastHTML = "<span>Playlist edits made</span>"+"<button class='btn-flat toast-action'>Undo</button>";
        M.toast({html: toastHTML, completeCallback: function() {
            if (!undoFlag) {

                dataToSend = {playlistId: playlistId, playlistName: $("#playlist-name-input").val(), visibility: $("#visibility").val()};
                $.post("/"+pathArray[1]+"/"+playlistId+"/edit", dataToSend, function(data) {
                    if (!data.success) {
                        $("#playlistName").text(oldName);
                        toastHTML = "<span>"+data.message+"</span>";
                        M.toast({html: toastHTML});
                    }
                })
            }
        }})

        $(".toast-action").click((e) => {
            undoFlag = true;
            $("#playlistName").text(oldName);
            M.Toast.dismissAll();
        })
    })

})

function attachRemoveEventListeners(playlist) {
    // Remove playlist item
    playlist.find('.remove').click(function(e){
        e.preventDefault();
        M.Toast.dismissAll();
        
        var parentNode = $(this).parent().parent();
        var videoId = $(this).attr("id");
        
        parentNode.hide();
        var undoFlag = false;

        toastHTML = "<span>Removed</span>"+"<button class='btn-flat toast-action'>Undo</button>";
        M.toast({html: toastHTML, completeCallback: function() {
            if (!undoFlag) {
                $.post("/"+pathArray[1]+"/"+playlistId+"/remove", {videoId: videoId, playlistId: playlistId}, function(data) {
                    if (data.success) {
                        parentNode.remove();
                    } else {
                        // Something must have went wrong
                        parentNode.show();
                        toastHTML = "<span>"+data.message+"</span>";
                        M.toast({html: toastHTML});
                    }
                })
            }
        }});
        
        $(".toast-action").click((e) => {
            undoFlag = true;
            parentNode.show();
            M.Toast.dismissAll();
        })

    });
}