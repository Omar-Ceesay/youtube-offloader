// $("#youtube-add").on("submit", function(e) {
//     e.preventDefault();
    
//     $.post("/youtube/add", {url: $("[name='youtubeUrl']").val()}, function(data) {
//         M.toast({html: data.message});
//         $("[name='youtubeUrl']").val("");
//     })
// })

function addToPlaylist(id, onPlaylistRoute) {
    if ($("#"+id).val().length === 0) {
        M.toast({html: "You have to submit a url"});
    } else {
        $("#progress"+id).toggle();
        $(id).toggle();
        $.post("/youtube/add", {url: $("[name='youtubeUrl"+id+"']").val(), playlistId: id}, function(data) {
            M.toast({html: data.message});
            $("[name='youtubeUrl"+id+"']").val("");
            $("#progress"+id).toggle();
            $(id).toggle();
            if (onPlaylistRoute && data.success) {
                $("#playlist").append('<li class="valign-wrapper playlist-item s12" title="' + data.videoInfo.video_id + '" name="' + data.videoInfo.title + '"><i class="material-icons reorder-icon handler">drag_indicator</i><div class="s10 ignore-drag"><span href="https://offtube.s3.amazonaws.com/' + data.videoInfo.video_id + '.mp3" class="purple-text text-darken-1 video-title">' + data.videoInfo.title + '</span></div><div class="ignore-drag s2 hide-on-small helper-buttons"><a class="waves-effect waves-green btn-small red darken-1 remove" id="' + data.videoInfo.video_id + '" title="Delete from playlist">remove</a></div><div class="ignore-drag s2 hide-on-med-and-up"><i class="material-icons reorder-icon" style="cursor: pointer;">chevron_left</i></div></li>')
                playlist = $('#playlist');
                attachRemoveEventListeners(playlist);
            }
        })
    }
}

$('input').keypress(function(event){
    if(event.keyCode == 13){
      $(this).next().click();
    }
});


function changeInputState(id) {
    $("#youtube-video-input"+id).toggle();
}