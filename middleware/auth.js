const userUtil = require("../util/user.js");
const Playlist = require('../models/playlist.js');
const fs = require("fs");

exports.isLoggedIn = function(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }else{
        res.redirect('/login');
    }
}

// If the user is logged in, they shouldn't be able to access X page
exports.isNotLoggedIn = function(req, res, next){
    if(req.isAuthenticated()){
        res.redirect('/');
    }else{
        return next();
    }
}

exports.isLoggedInVar = function(req, res, next){
    res.locals.login = req.isAuthenticated();
    next();
}

exports.sendUserInfo = function(req, res, next) {
    res.locals.user = req.user;
    if (req.user) {
        let playlists = userUtil.getUserPlaylists(req.user.id);
        playlists.then((data) => {            
            if (!data) {
                res.locals.playlists = [];
                next();
            } else {
                res.locals.playlists = data;
                next();
            }
        }, (err) => {
            console.log(err);
            res.locals.playlists = [];
            next();
        })
    } else {
        next();
    }
}

exports.authorizePlaylist = function(req, res, next) {
    Playlist.findById(req.params.id, (err, playlist) => {
        if (!playlist) {
            res.locals.playlist = null;
            res.locals.message = "Couldn't find a playlist by that id";
            next();
        } else if (playlist.visibility === "public") {
            res.locals.playlist = playlist;
            next();
        } else if (typeof req.user !== "undefined" && playlist.owner.id === req.user.id) {
            res.locals.playlist = playlist;
            next();
        } else {
            res.locals.playlist = null;
            res.locals.message = "You do not have access to see this playlist";
            next();
        }
    })
}

exports.authorizePlaylistUpdate = function(req, res, next) {
    Playlist.findById(req.body.playlistId, (err, playlist) => {
        if (!playlist) {
            next(new Error("No playlist by that id found"));
        } else if (typeof req.user === "undefined" || playlist.owner.id !== req.user.id) {
            res.send({message: "You are not authorized to edit this playlist"});
        } else {
            next();
        }
    })
}