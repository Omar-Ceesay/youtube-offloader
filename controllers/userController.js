const passport = require('passport');
const Promise = require("bluebird");

let Playlist = require('../models/playlist.js');
let User = require('../models/user');
const moment = require("moment");

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

exports.user_list = function(req, res, next){
    res.render("./index", {videos: []});
}

exports.user_login = function(req, res, next){
    res.render("./login");
}

exports.user_signup = function(req, res, next){
    res.render("./signup");
}

exports.user_logout = function(req, res, next){
    req.logout();
    res.redirect('/');
}

exports.create_playlist = function(req, res, next) {    
    if (typeof req.user === "undefined") return res.send({message: "Please signin before creating playlists"});
    if (typeof req.body.playlistName === "undefined" || req.body.playlistName.length === 0) return res.send({message: "You must submit a name new playlists"});

    User.findById(req.user.id, (err, user) => {
        if (err) {
            console.log(err);
            res.send({message: "Something went wrong"});
        } else if (!user) {
            res.send({message: "Please signin before creating playlists"});
        } else {            
            let playlist = new Playlist();
            playlist.owner.username = req.user.username;
            playlist.owner.email = req.user.email;
            playlist.owner.id = req.user.id;
            playlist.createdOn = moment().format('MMMM Do YYYY, h:mm:ss a');
            playlist.visibility = "private";
            playlist.playlistInfo = {
                name: req.body.playlistName,
                videoCount: 0
            }

            playlist.save((err) => {
                if (err) {
                    console.error(err);
                    res.send({message: "Something went wrong"});
                } else {
                    res.send({message: "Created playlist: " + req.body.playlistName});
                }
            })
        }
    })
    
}

exports.add_to_playlist = function(videoInfo, userId, playlistId) {
    return new Promise((resolve, reject) => {
        Playlist.findById(playlistId, (err, playlist) => {
            artist = videoInfo.title.substr(0, videoInfo.title.indexOf(' -'));
            if (videoInfo.title.substr(0, videoInfo.title.indexOf(' -')).length === 0) {
                artist = videoInfo.title.substr(0, videoInfo.title.indexOf('-'))
            }
            playlist.items.push({
                id: videoInfo.video_id,
                title: videoInfo.title,
                name: videoInfo.title,
                image: videoInfo.player_response.videoDetails.thumbnail.thumbnails[3].url,
                artist: artist
            })
            Playlist.findByIdAndUpdate(playlist.id, { 
                items: playlist.items,
                'playlistInfo.videoCount': (playlist.playlistInfo.videoCount+1)
            }, (err, doc) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        })
    })
}

exports.remove_from_playlist = function(playlistId, videoId) {
    return new Promise((resolve, reject) => {
        Playlist.findById(playlistId, (err, playlist) => {
            if (err) {
                reject(err);
            } else if (!playlist) {
                reject("This playlist couldn't be found");
            } else {
                var items = playlist.items;
                var index = items.findIndex(elem => elem.id === videoId);
                items.splice(index, 1);

                Playlist.findByIdAndUpdate(playlist.id, {'$set': {
                    'playlistInfo.videoCount': items.length,
                    items: items
                }}, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                })
            }
        })
    })
}

exports.edit_playlist = function(playlistId, playlistName, visibility) {
    return new Promise((resolve, reject) => {
        Playlist.findByIdAndUpdate(playlistId, {'$set': {
            'playlistInfo.name': playlistName,
            visibility: visibility
        }}, (err, playlist) => {
            if (err) {
                reject(err);
            } else if (!playlist) {
                reject("This playlist couldn't be found");
            } else {
                resolve();
            }
        })
    })
}

exports.sort_playlist = function(playlistId, items) {
    var updatedItems = [];

    // This whole algorithem is terrible resource wise. Just look at the commented out logs...

    return new Promise((resolve, reject) => {
        Playlist.findById(playlistId, (err, playlist) => {
            if (items.length !== playlist.items.length) {
                // incorrect items list sent
                console.log("Incorrect items length!");
            } else {
                Promise.each(items, function(itemPromise, i) {
                    // console.log("ON: "+itemPromise);
                    Promise.each(playlist.items, function(playlistItem, j) {
                        // console.log("Checking: "+playlist.items[j].id);
                        if (itemPromise === playlist.items[j].id) {
                            updatedItems.push(playlistItem);
                            // console.log("pushing: "+playlistItem.title);
                        }
                    })
                }).then(() => {
                    if (updatedItems.length !== playlist.items.length) {
                        resolve({message: "Something went wrong, please try again", success: false});
                    } else {
                        Playlist.findByIdAndUpdate(playlistId, {items: updatedItems}, (err) => {
                            if (err) {
                                resolve({message: "Something went wrong, please try again", success: false});
                            } else {
                                resolve({message: null, success: true});
                            }  
                        })
                    }
                })
            }
        })
    })
}

exports.get_playlists = function(req, res, next) {
    try {
        if (!req.user) {
            next();
        } else {
            Playlist.find({ 'owner.id': req.user.id }, (err, playlists) => {
                if (err) {
                    console.log(err);
                    res.locals.playlists = [];
                    next();
                } else {
                    res.locals.playlists = playlists;
                    next();
                }
            });
        }

    } catch (err) {
        console.log(err);
        res.locals.playlists = [];
        next();
    }
}