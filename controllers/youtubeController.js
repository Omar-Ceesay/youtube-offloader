const fs = require('fs');
const ytdl = require('ytdl-core');

let user_controller = require("../controllers/userController");

let videoUitl = require("../util/video.js");
var awsUtil = require("../util/aws.js");
var ffmpegUtil = require("../util/ffmpeg.js");

exports.addOne = function(req, res, next){
    url = new URL(req.body.url);
    if (url.pathname === "/playlist") {
        res.send({success: false, message: "That's a playlist buddy"});
    } else {
        // for getting video_id, title, etc
        ytdl.getInfo(req.body.url, (err, info) => {
            if (err) {
                console.log(err)
                res.send({message: err.message});
            } else {
    
                var audio = ytdl(req.body.url, { quality: "highest" });
    
                // Send progress back to client with Socket.io
                audio.on("response", (res) => {
                    var totalSize = res.headers['content-length'];
                    var dataRead = 0;
                    res.on('data', function(data) {
                        dataRead += data.length;
                        var percent = dataRead / totalSize;
                        // Can't use these on Herokuu
                        // process.stdout.cursorTo(0);
                        // process.stdout.clearLine(1);
                        // process.stdout.write((percent * 100).toFixed(2) + '% ');
                    });
                    res.on('end', function() {
                        // process.stdout.write('\n');
                    });
                })
    
    
                let filename = info.video_id+'.mp4';
                let checkInBucket = awsUtil.isInS3(info.video_id+".mp3");
    
                checkInBucket.then((doesFileExist) => {
                    if (doesFileExist) {
                        let add_to_playlist = user_controller.add_to_playlist(info, req.user.id, req.body.playlistId);
                        add_to_playlist.then(() => {
                            try {
                                videoUitl.increment(info.video_id, info.title);
                            } catch (err) {
                                console.log(err);
                            }
                            res.send({success: true, message: "Updated your playlist", videoInfo: {
                                title: info.title,
                                video_id: info.video_id
                            }});
                        }, (err) => {
                            console.error(err);
                            res.send({success: false, message: "Failed to add this track to your playlist."});
                        })
                        
                    } else {
                        let tempFile = fs.createWriteStream(filename);
                        audio.pipe(tempFile);
    
                        tempFile.on("close", () => {
                            // convert video format to mp3
                            let convert = ffmpegUtil.convertToMp3(filename, info.video_id);
                            
                            convert.then(() => {
                                // Delete the video file
                                fs.unlink(filename, (err) => {
                                    if (err) console.log(err);
                                });
                                // rename filename to the file with .mp3
                                filename = info.video_id+".mp3";
                                let uploader = awsUtil.uploader(filename);
                                uploader.then((uploadedCorrectly) => {
                                    
                                    let add_to_playlist = user_controller.add_to_playlist(info, req.user.id, req.body.playlistId);
                                    add_to_playlist.then(() => {
                                        try {
                                            videoUitl.increment(info.video_id, info.title);
                                        } catch (err) {
                                            console.log(err);
                                        }
                                        res.send({success: true, message: "Download successful & added to your playlist", videoInfo: {
                                            title: info.title,
                                            video_id: info.video_id
                                        }});
                                    }, (err) => {
                                        console.error(err);
                                        res.send({success: false, message: "Failed to add this track to your playlist."});
                                    })
            
                                }, (err) => {
                                    console.error(err);
                                    res.send({success: false, message: "Failed to upload audio file."});
                                })
                            }, (err) => {
                                fs.unlink(filename, (err) => {
                                    if (err) console.log(err);
                                });
                                console.log(err);
                                res.send({success: false, message: "Something went wrong converting this track"});
                            })
    
                        })
                    }
                }, (err) => {
                    console.log(err);
                    res.send({success: false, message: "Couldn't comfirm whether this exists in bucket."});
                })
            }
            
        });
    }
        
    


}
