const LocalStrategy = require('passport-local').Strategy;
let User = require('../models/user');
const bcrypt = require('bcryptjs');
const moment = require("moment");

module.exports = function(passport){

    passport.serializeUser(function(user, done){
        if (user) {
            done(null, user._id);
        } else {
            done(null, req.flash("WOW SO FAIL"));
        }
    });

    passport.deserializeUser(function(id, done){
        User.findById(id, function(err, user){
            done(err, user);
        })
    });

    passport.use('local-signup', new LocalStrategy({
        passwordField: "password",
        passReqToCallback: true,
        usernameField: 'email'
    },
    
        function(req, email, password, done){
            process.nextTick(function(){
                email = email.toLowerCase();

                User.findOne({email: email}, function(err, person){
                    if (person) {
                        return done(null, false, {message: "That email is already in use."});
                    } else if(password.length == 0 || !password) {
                        return done(null, false, {message: "You must submit a password."});
                    } else if (!person) {
                        let newUser = new User();

                        bcrypt.genSalt(10, function(err, salt) {
                            bcrypt.hash(password, salt, function(err, hash){
                                newUser.email = email.toLowerCase();
                                newUser.password = hash;
                                newUser.username = req.body.name;
                                newUser.createdOn = moment().format('MMMM Do YYYY, h:mm:ss a');
                                newUser.playlists = [];
                                newUser.save(function(err){
                                    if (err) console.error(err);
                                    else {
                                    }
                                    return done(null, newUser);
                                })
                            })
                        })
                    }
                })
            })
        }
    ))
    
    passport.use('local-login', new LocalStrategy({
        passwordField: "password",
        passReqToCallback: true,
        usernameField: 'email'
    }, function(req, email, password, done) {
        email = email.toLowerCase();
        process.nextTick(function() {
            User.findOne({email: email}, function(err, person){                
                if(!person) {
                    return done(null, false, {message: "A user with that email couldn't be found", status: false});
                } else if (!password) {
                    return done(null, false, {message: "You have to submit a password to login...", status: false});
                } 

                bcrypt.compare(password, person.password, function(err, res) {
                    
                    if (err){
                        return done(err, false);
                    } else if (!res) {
                        return done(null, false, {message: "Incorrect password", status: false});
                    } else {
                        return done(null, person)
                    }
                });
            })
        })
    }))
}