let user_controller = require("../controllers/userController");

let auth = require("../middleware/auth");

module.exports = function(app, passport){

    app.get("/", user_controller.get_playlists, user_controller.user_list);

    app.get("/login", auth.isNotLoggedIn, user_controller.user_login);

    app.post("/login", function(req, res, next) {
        passport.authenticate('local-login', function(err, user, info) {            
            if (err) {
                console.log(err);
                return res.render("login", {message: "Something went wrong, please try again"});
            }
            if (!user) return res.render("login", info);

            req.logIn(user, function(err) {
                if (err) {
                    console.log(err);
                    return res.render("login", {message: "Something went wrong logging you in. Please try again"});
                } else {
                    return res.redirect("/");
                }
            });
        })(req, res, next)
    });

    app.get("/signup", auth.isNotLoggedIn, user_controller.user_signup);

    app.post("/signup", function(req, res, next) {
        passport.authenticate('local-signup', function(err, user, info) {
            if (err) return res.render("signup", {message: "Something went wrong, please try again"});
            if (!user) return res.render("signup", info);

            req.logIn(user, function(err) {
                if (err) { 
                    console.log(err);
                    return res.send({message: "Something went wrong"});
                } else {
                    return res.redirect("/");
                }
            });
        })(req, res, next)
    });

    app.get("/logout", user_controller.user_logout);

    app.post("/createplaylist", user_controller.create_playlist);
    
}
