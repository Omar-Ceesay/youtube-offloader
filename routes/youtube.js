let youtube_controller = require("../controllers/youtubeController");

let auth = require("../middleware/auth");

module.exports = function(app, passport){

    app.post("/add", auth.isLoggedIn, auth.authorizePlaylistUpdate, youtube_controller.addOne);
}