let auth = require("../middleware/auth");
let playlist_controller = require("../controllers/playlistController");
let user_controller = require("../controllers/userController");
let videoUtil = require("../util/video.js");

module.exports = function(app){
    app.get("/:id", auth.authorizePlaylist, (req, res) => {
        res.render("playlist");
    })
    // REMOVE THIS ROUTE BUDDY
    app.get("/updatecounts/all", (req, res) => {
        videoUtil.allCountsUpdate();
        res.send("Remove this route buddy")
    })

    app.post("/:id/remove", auth.authorizePlaylist, auth.authorizePlaylistUpdate, (req, res) => {
        let remove_from_playlist = user_controller.remove_from_playlist(req.body.playlistId, req.body.videoId);

        remove_from_playlist.then(() => {
            videoUtil.decrement(req.body.videoId);
            res.send({message: "Removed", success: true});
        }, (err) => {
            res.send({message: "Something went wrong!", success: false});
        })
    })

    app.post("/:id/edit", auth.authorizePlaylist, auth.authorizePlaylistUpdate, (req, res) => {
        let edit_playlist = user_controller.edit_playlist(req.body.playlistId, req.body.playlistName, req.body.visibility);

        edit_playlist.then(() => {
            res.send({message: "Saved!", success: true});
        }, (err) => {
            res.send({message: err.message, success: false});
        })
    })

    app.post("/:id/sort", auth.authorizePlaylist, auth.authorizePlaylistUpdate, (req, res) => {
        new Promise((resolve, reject) => {
            promiseList = [];

            for (let i = 0; i < req.body.items.length; i++) {
                const element = req.body.items[i];
                promiseList.push(Promise.resolve(element));

                if(i === req.body.items.length-1) {
                    resolve(promiseList);
                }
            }
        }).then((promiseList) => {
            let sort_playlist = user_controller.sort_playlist(req.body.playlistId, promiseList);
    
            sort_playlist.then((data) => {
                res.send(data);
            }, (err) => {
                console.log(err);
            })
        })
    })
}