const AWS = require('aws-sdk');
const s3Bucket = process.env.s3Bucket || "offtube";
const fs = require('fs');

AWS.config.update({region: 'us-east-1'});
AWS.config.accessKeyId = process.env.AWS_ACCESSKEYID || "AKIA4ZQPU7XZ3CYXR6YB";
AWS.config.secretAccessKey = process.env.AWS_SECRET || "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";
const s3 = new AWS.S3({apiVersion: '2006-03-01'});

// if the file "name" is located in 
exports.isInS3 = function(name) {
    return new Promise((resolve, reject) => {
        var params = {
            Bucket: s3Bucket,
            Key: name
        }
    
        s3.getObject(params, (err, data) => {
            if (err === null) {
                resolve(true);
            } else if (err.code === "NoSuchKey") {                
                resolve(false);
            } else if (err) {
                reject(err);
            } 
        })
    })
}

exports.uploader = function(filename) {
    return new Promise((resolve, reject) => {
        var fileStream = fs.createReadStream(filename);

        var uploadParams = {
            Bucket: s3Bucket,
            Key: filename,
            Body: fileStream,
            ContentType: "audio/mpeg"
        }


        s3.upload(uploadParams, (err, data) => {
            if (err) reject("File couldn't be uploaded to S3");
            else {
                resolve(true);
            }
            // Regardless of whether or not the file was uploaded successfully,
            // I want to delete it from the disk
            fs.unlink(filename, (err) => {
                if (err) {
                    console.log("ERROR: The file couldn't be deleted!");
                    console.log(err);
                }
            })

        })
    })
}