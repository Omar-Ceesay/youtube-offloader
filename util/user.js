let User = require("../models/user")

exports.getUserPlaylists = function(userId) {
    return new Promise((resolve, reject) => {
        User.findById(userId, (err, user) => {
            if (err) {
                reject(err);
            } else if (!user) {
                resolve(null);
            } else {
                resolve(user.playlists);
            }
        })
    })
}