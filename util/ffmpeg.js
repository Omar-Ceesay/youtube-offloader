var ffmpeg = require('ffmpeg');

exports.convertToMp3 = function(filename, video_id) {
    return new Promise((resolve, reject) => {
        var process = new ffmpeg(filename);
        process.then((video) => {
            video.fnExtractSoundToMP3(video_id+".mp3", (err, file) => {
                if (err) {
                    console.log(err);
                    reject();
                } else {
                    resolve();
                }
            })
        })
    })
}