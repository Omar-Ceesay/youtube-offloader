let Video = require("../models/video");
let Playlist = require("../models/playlist");

exports.increment = function(videoId, title) {
    Video.findOneAndUpdate({videoId: videoId, title: title}, { $inc: { count: 1 } }, {
        new: true,
        upsert: true
    }, (err) => {
        if (err) throw err;
    })
}

exports.decrement = function(videoId) {
    Video.findOneAndUpdate({videoId: videoId}, { $inc: { count: -1 } }, (err) => {
        if (err) throw err;
    })
}
async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
exports.allCountsUpdate = function() {
    var allvideos = {};
    const start = async () => {
        await Playlist.find({}, (err, playlists) => {
            asyncForEach(playlists, async (playlist, i) => {
                asyncForEach(playlist.items, async (item, j) => {
                    if (!allvideos[item.id]) {
                        allvideos[item.id] = {count: 1, id: item.id, name: item.name};
                    } else {
                        allvideos[item.id].count++;
                    }

                    if (i >= playlists.length-1 && j >= playlist.items.length-1) {
                        updateVideo();
                    }
                })
            })
        })
        function updateVideo() {
            Video.find({}, (err, videos) => {
                asyncForEach(videos, async (video) => {
                    if (allvideos[video.videoId] && allvideos[video.videoId].count != video.count) {
                        Video.findByIdAndUpdate(video.id, {count: allvideos[video.videoId].count}, (err, updatedVideo) => {
                            console.log("updating "+allvideos[video.videoId].name+" to "+allvideos[video.videoId].count);
                        });
                    } else if (!allvideos[video.videoId] && video.count != 0) {
                        Video.findByIdAndUpdate(video.id, {count: 0}, (err, updatedVideo) => {
                            console.log("updating "+video.title+" to 0");
                        });
                    }
                })
            })
        } 
    }
    start();
}